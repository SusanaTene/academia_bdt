import sqlite3


class BD:

    def __init__(self):
        self.conexion = sqlite3.connect('academia/academia.sqlite')
        self.cursor = None
        print("Conexion establecida !!!")

    def crear_bd(self):
        sql_user = """CREATE TABLE User (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
        name   TEXT UNIQUE, 
        email  TEXT)"""
        sql_course = """ CREATE TABLE Course (
        id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        title   TEXT UNIQUE
        )"""
        sql_member = """ CREATE TABLE Member (
        user_id     INTEGER,
        course_id   INTEGER,
        role        INTEGER,
        PRIMARY KEY (user_id, course_id)
        ) """

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_user)
        self.cursor.execute(sql_course)
        self.cursor.execute(sql_member)
        print("Creadas las tablas !!!")
        self.cursor.close()

    def insertar_users(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO User(name, email) VALUES(?, ?)",
                                [('Ingrid', 'ingrid.guaraca@unl.edu.ec'),
                                 ('Michael', 'michael.vasquez@unl.edu.ec'),
                                 ('Juan', 'juan.v.alvarez@unl.edu.ec')])
        self.conexion.commit()
        self.cursor.close()
        print ("Usuarios insertados")

    def insertar_courses(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO Course(title) VALUES(?)",
                                [('Programación',),
                                 ('Evaluación de los Aprendizajes',),
                                 ('Educación No Formal',)])
        self.conexion.commit()
        self.cursor.close()
        print ("Cursos insertados")

    def insertar_members(self): #Matricular
        self.cursor = self.conexion.cursor()
        try:
            self.cursor.executemany("INSERT INTO Member(user_id, course_id, role) VALUES(?, ?, ?)",
                                [(1, 1, 1),
                                 (2, 1, 0),
                                 (3, 1, 0),
                                 (1, 2, 0),
                                 (2, 2, 1),
                                 (2, 3, 1),
                                 (3, 3, 0)
                                ])
            self.conexion.commit()
            self.cursor.close()
            print("Miembros insertados")

        except sqlite3.Error as ex:
            print ("Error: ", ex)


    def consultar_datos(self):
        sql = """SELECT User.name, Member.role, Course.title
                FROM User JOIN Member JOIN Course
                ON Member.user_id = User.id AND 
                Member.course_id = Course.id
                ORDER BY Course.title, Member.role DESC, User.name """
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        filas = self.cursor.fetchall()
        for f in filas:
            print (f)
        self.cursor.close()

if __name__ == "__main__":
    base = BD()
    base.crear_bd()
    #base.insertar_users()
    #base.insertar_members()
    #base.consultar_datos()

